// define libraries
const axios = require('axios');
const cheerio = require('cheerio');
const express = require('express');
const ExportToCSV = require('export-to-csv').ExportToCsv;
const fs = require('fs');

// define constants
const app = express();

// get top 100 handphones from tokopedia with rating 4+ and has reviews
app.get('/handphone', async (req, res) => {
    try
    {
        // define variables
        let arrInfo = [];
        let ctr = 1;
    
        // loop page
        for(x=1; x<=10; x++)
        {
            let url = 'https://www.tokopedia.com/search?ob=5&page='+x+'&rt=4%2C5&st=product&q=handphone';

            let response = await axios(url);
            let html = response.data;
            let $ = cheerio.load(html);
            let ctrPage = 1;

            // get all the data
            $('.css-12sieg3', html).each(function() {
                // somehow, data valid only in the first 10 data/page. need to work around this again in the future.
                if(ctrPage <= 10)
                {
                    let item = $(this).find('.css-1f4mp12').text();
                    let photoURL = $(this).find('.css-1ehqh5q').find('img').attr('src');
                    let price = $(this).find('.css-rhd610').text();
                    let rating = $(this).find('.css-etd83i').text();
                    let location = $(this).find('.css-1rn0irl').find('[data-testid="spnSRPProdTabShopLoc"]').text();
                    let store = $(this).find('.css-1rn0irl').find('[data-testid=""]').text();

                    // push them to array
                    arrInfo.push({
                        no: ctr,
                        item,
                        photoURL,
                        price,
                        rating,
                        merchant: store+' ('+location+')'
                    });

                    ctrPage++;
                    ctr++;
                }
            });
        }

        // export to csv
        let opts = {
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalSeparator: '.',
            showLabels: true, 
            showTitle: true,
            title: 'Top 100 Handphone',
            useTextFile: false,
            useBom: true,
            useKeysAsHeaders: true,
        }

        let exportCSV = new ExportToCSV(opts);
        let csvData = exportCSV.generateCsv(arrInfo, true);
        
        // save csv file to local. need to work around for downloadable csv
        fs.writeFileSync('my_data.csv', csvData);

        res.json(arrInfo);
    }
    catch(err)
    {
        console.log(err);
    }
});

app.listen(3010, () => {
    console.log(`Listening on http://localhost:3010`);
});